# Whatsapp Sender

## Description
Implementation of [whatsapp-web.js](https://github.com/pedroslopez/whatsapp-web.js) on standalone REST API

# Features
- Login by scan qrcode that appear in terminal
- Send text message to provided phone number
- Whatsapp connection status checking

## Installation
- Clone this repo
- Set env and rename .env.example to .env
- Run "npm install"
- Run "npm start"

## API Spefication
-

## Authors and Acknowledgment
Dimas Setiawan (dimasdhimek@gmail.com)

## License
This program is free software.
It is licensed under the GNU GPL version 3 or later.
That means you are free to use this program for any purpose;
free to study and modify this program to suit your needs;
and free to share this program or your modifications with anyone.
If you share this program or your modifications
you must grant the recipients the same freedoms.
To be more specific: you must share the source code under the same license.
For details see https://www.gnu.org/licenses/gpl-3.0.html
