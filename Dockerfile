FROM zenika/alpine-chrome:with-puppeteer

WORKDIR /app

USER chrome

COPY --chown=chrome:chrome package*.json /app

RUN npm ci

COPY --chown=chrome:chrome . /app

RUN npm run build

CMD [ "node", "./dist/index.js" ]
