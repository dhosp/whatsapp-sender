import { sendMessage, status, reconnect } from "../helpers/WaWeb";
import Response from "../helpers/Response";

class WAController {
  static async sendMessage(req, res, next) {
    try {
      const status = await sendMessage(req.body.phone, req.body.message);
      if (status) {
        return res.json(Response.success("Sukses"));
      } else {
        return res.status(500).json(Response.error("Gagal"));
      }
    } catch (error) {
      next({ error, fun: "WA:sendMessage" });
    }
  }

  static async checkStatus(req, res, next) {
    try {
      const wastatus = await status();
      if (wastatus) {
        return res.json(Response.success("Terhubung"));
      } else {
        return res.status(500).json(Response.error("Diskonek"));
      }
    } catch (error) {
      next({ error, fun: "WA:checkStatus" });
    }
  }

  static async reconnect(req, res, next) {
    try {
      const status = await reconnect();
      if (status) {
        return res.json(Response.success("Terhubung"));
      } else {
        return res.status(500).json(Response.error("Diskonek"));
      }
    } catch (error) {
      next({ error, fun: "WA:reconnect" });
    }
  }
}

module.exports = WAController;
