import { Client, LocalAuth } from "whatsapp-web.js";
import qrcode from "qrcode-terminal";
import axios from "axios";
import logger from "./Logger";

let status = false;

const client = new Client({
  authStrategy: new LocalAuth({
    dataPath: "./sessions"
  }),
  webVersionCache: {
    type: "remote",
    remotePath:
      "https://raw.githubusercontent.com/wppconnect-team/wa-version/main/html/2.2412.54.html",
  },
  puppeteer: { headless: true, args: ['--no-sandbox'], },
});

client.initialize();

client.on("qr", (qr) => {
  qrcode.generate(qr, { small: true });
  console.log("QR RECEIVED", qr);
});

client.on("authenticated", () => {
  console.log("AUTHENTICATED");
});

client.on("auth_failure", (msg) => {
  console.log("AUTHENTICATION FAILURE", msg);
  status = false;
});

client.on("ready", () => {
  status = true;
  console.log("READY");
});

client.on("change_state", (state) => {
  console.log("CHANGE STATE", state);
});

client.on('message', async (message) => {
  try {
    await axios.post(process.env.CALLBACK_URL, message, { headers: { "X-API-KEY": process.env.CALLBACK_KEY } });
  } catch (error) {
    logger.error('Callback failed to send');
    logger.error(error);
  }
});

client.on("disconnected", (reason) => {
  console.log("LOGGED OUT", reason);
  status = false;
});

class WaWeb {
  static async sendMessage(phoneNumber, message) {
    if (!status) {
      for (let i = 0; i < 3; i++) {
        if (status) {
          break;
        } else {
          await client.initialize();
        }
      }
    }

    if (status) {
      if (phoneNumber[0] == 0) {
        phoneNumber = phoneNumber.replace("0", "62");
      }
      const result = await client.sendMessage(phoneNumber + "@c.us", message);
      if (result.id.fromMe) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  static async reconnect() {
    if (!status) {
      await client.initialize();
    }
    return status;
  }

  static async status() {
    return status;
  }
}

module.exports = WaWeb;
