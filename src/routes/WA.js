import { Router } from "express";
import {
	checkStatus,
	sendMessage,
	reconnect,
} from "../controllers/WAController";

const routes = Router();

routes.post("/send", sendMessage);
routes.get("/status", checkStatus);
routes.post("/reconnect", reconnect);

module.exports = routes;
