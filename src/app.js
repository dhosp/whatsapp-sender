import Compression from "compression";
import Cors from "cors";
import "dotenv/config";
import express from "express";
import morgan from "morgan";
import logger from "./helpers/Logger";

const app = express();
const port = process.env.PORT || 3000;

app.use(
  Cors(),
  express.json({
    limit: "10mb",
  }),
  express.urlencoded({
    limit: "10mb",
    extended: true,
  }),
  Compression()
);

if (process.env.NODE_ENV !== "production") app.use(morgan("dev"));

app.use((req, res, next) => {
  if (req.get("X-API-KEY") != process.env.API_KEY) {
    return res.status(401).json({ status: false, message: "Unauthorized" });
  } else {
    next();
  }
});

app.use(require("./routes/WA"));

app.use(function (error, req, res, next) {
  logger.error(error.error);
  logger.info(
    JSON.stringify({
      fun: error.fun,
      user: req.user ?? {},
      body: req.body ?? {},
    })
  );

  if (process.env.NODE_ENV != "production") {
    console.log(error);
  }

  return res.status(500).json({
    message: "Terjadi kesalahan saat mengolah data",
    status: false,
  });
});

app.use(function (req, res, next) {
  return res.send("404 Page not found!");
});

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

export default app;
